import React from "react";
import Layout from "../components/layout"
import SEO from "../components/seo"
import {ContactHeader,Banner} from '../utils/'
import contactImg from '../images/bcg/contactBcg.jpeg'


const ContactPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <ContactHeader img={contactImg}>
    <Banner title ="Contact us" subtitle="feel free to reach out to us for any query"/>

    </ContactHeader>


  </Layout>
)
// const ButtonWrapper = styled.button`
 
// `;
export default ContactPage
