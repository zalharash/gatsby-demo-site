import React from "react";
import Layout from "../components/layout"
import SEO from "../components/seo"
import {MenuHeader,Banner} from '../utils/'
import menuImg from '../images/bcg/menuBcg.jpeg'


const MenuPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <MenuHeader img={menuImg}>
    <Banner title ="Menu" subtitle={`let's dig in`}/>
    </MenuHeader>


  </Layout>
)
// const ButtonWrapper = styled.button`
 
// `;
export default MenuPage
