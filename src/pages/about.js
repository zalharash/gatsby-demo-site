import React from "react";
import Layout from "../components/layout"
import SEO from "../components/seo"
import {AboutHeader,Banner} from '../utils/'
import aboutImg from '../images/bcg/aboutBcg.jpeg'


const AboutPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <AboutHeader img={aboutImg}>
    <Banner title ="About us" subtitle="A brief about us"/>

    </AboutHeader>


  </Layout>
)
// const ButtonWrapper = styled.button`
 
// `;
export default AboutPage
