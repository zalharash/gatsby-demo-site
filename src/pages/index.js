import React from "react";
import Layout from "../components/layout"
import SEO from "../components/seo"
import { HomeHeader, Banner, BannerButton } from '../utils/'
import homeBcg from '../images/bcg/homeBcg.jpeg'
import QuickInfo from '../components/HomePageCmp/QuickInfo'
import Gallery from '../components/HomePageCmp/Gallery-1'
import Menu from '../components/HomePageCmp/Menu'

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <HomeHeader img={homeBcg}>
      <Banner title="Damas" subtitle="Enjoy the best taste from the republic of taste">
        {/* To horizontally center a block element (like div), use margin: auto */}
        <BannerButton style={{ margin: '2rem auto' }}>Menu</BannerButton>
      </Banner>
    </HomeHeader>
    <QuickInfo />
    <Gallery />
    <Menu />
  </Layout>
)
// const ButtonWrapper = styled.button`

// `;
export default IndexPage
