import React, { Component } from 'react'
import { Link } from 'gatsby';
import logo from '../../../images/logo.svg';
import { FaAlignRight } from 'react-icons/fa';
import styled from 'styled-components';
import {styles} from '../../../utils';

export default class NavbarHeader extends Component {
  render() {
    const { handleNavbar } = this.props;    
    return (
      <HeaderWrapper>
      <Link to="/">
      <img src={logo} alt="DamasResturan"/>
      </Link>
      <FaAlignRight className="toggle-icon" onClick={handleNavbar}/>
      </HeaderWrapper>
    )
  }
}

const HeaderWrapper = styled.div`
/* use flex display to be able to align them on the center on vertic and Horz
use justify-content with space between to add as much space as possible in between */
padding:0.4rem 1rem;
display:flex;
align-items:center;
justify-content: space-between;

.toggle-icon{
  /* rem is releative size to the root, so 1.75 means 1.75 time bigger than
  the font size of the root */
  font-size:1.75rem;
  color:${styles.colors.mainYellow};
  cursor:pointer;
}
@media (min-width:768px){
  .toggle-icon{
    display: none;    
  }    
  }
`;