import React, { Component } from 'react'
import { FaInstagram, FaTwitter, FaFacebook } from 'react-icons/fa';
import styled from 'styled-components';
import { styles } from '../../../utils';

export default class NavbarIcons extends Component {
  state = {
    icons: [
      {
        id: 1,
        path: 'https://gby-demo-zhrsh.netlify.com/',
        icon: <FaFacebook className="icon facebook-icon" />
      },
      {
        id: 2,
        path: 'https://gby-demo-zhrsh.netlify.com/',
        icon: <FaTwitter className="icon twitter-icon" />
      },
      {
        id: 3,
        path: 'https://www.instagram.com/',
        icon: <FaInstagram className="icon instagram-icon" />
      }
    ]
  }
  render() {
    const iconMap = this.state.icons.map(icon => {
      return (
        <a href={icon.path} key={icon.id} target="_blank" rel="noopener noreferrer">{icon.icon}</a>
      )
    }
    )

    return (
      <IconWrapper>
        {iconMap}
      </IconWrapper>
    )
  }
}

const IconWrapper = styled.div`
/* Hide on small screen */
display:none;
@media (min-width:768px) {
/* show on big screen */
display:flex;
width: 10rem;
justify-content:space-around;
}
  
  .icon{
  font-size:2rem;  
  cursor:pointer;
  ${styles.transFunction(undefined, '0.4s')};
  &:hover{
    color:${styles.colors.mainYellow};
  }
}
.facebook-icon{
color:#3b579d;
}
.instagram-icon{
  color:#da5f53;
}
.twitter-icon{
  color:#3ab7f0;
}

`;