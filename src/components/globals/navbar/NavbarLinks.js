import React, { Component } from 'react'
import { Link } from 'gatsby';
import styled from 'styled-components';
import { styles } from '../../../utils';


export default class NavbarLinks extends Component {
  state = {
    Links: [
      {
        id: 0,
        path: '/',
        name: 'home'
      },
      {
        id: 1,
        path: '/about/',
        name: 'about'

      },
      {
        id: 2,
        path: '/menu/',
        name: 'menu'

      },
      {
        id: 3,
        path: '/contact/',
        name: 'contact'

      }
    ]
  }
  render() {
    return (
      <LinkWrapper navOpen={this.props.navbarOpen}>
        {
          this.state.Links.map(item => {
            return (
              <li key={item.id}>
                <Link to={item.path} className="nav-link">
                  {item.name}
                </Link>
              </li>
            )
          }
          )
        }
      </LinkWrapper>
    )
  }
}

/******************************/
//   CSS Styled Components
/******************************/
const LinkWrapper = styled.ul`
li{
  list-style-type:none;
}
.nav-link{
  display:block;
  /* to remove the underline from Link */
  text-decoration:none;
  padding: 0.5rem 1.2rem 0.5rem 1.2rem;
  color:${styles.colors.mainGrey};
  font-size:1.2rem;
  font-weight:700;
  text-transform:capitalize;
  cursor:pointer;
  /* ${styles.transDefault}; */
  transition:all 0.5s linear;
  /* nested hover of the nav-link */
  &:hover{
    background:${styles.colors.mainGrey};
    color:${styles.colors.mainYellow};
    /* as I am hovering shift it to right by adding padding to left */
    padding:0.5rem 1.2rem 0.5rem 1.6rem;
  }
}
/* Pass props from to styled components */
height:${props=>(props.navOpen? '152px' :'0px')};
overflow:hidden;
${styles.transObject({time:'0.7s'})};

@media (min-width:768px) {
  font-size:1.4rem;
  height:auto;
  display:flex;
  margin: 0 auto;  
  .nav-link:hover{
    background:${styles.colors.mainWhite};
    padding:0.5rem 1.2rem 0.5rem 1.2rem;

  }
}


`;