import React, { Component } from 'react'
import styled from 'styled-components';
import { styles } from '../../utils';
import { FaInstagram, FaTwitter, FaFacebook } from 'react-icons/fa';

export default class Footer extends Component {
    state = {
        icons: [
            {
                id: 1,
                icon: <FaFacebook className="icon facebook-icon" />,
                path: 'https://gby-demo-zhrsh.netlify.com/'
            },
            {
                id: 2,
                icon: <FaTwitter className="icon twitter-icon" />,
                path: 'https://gby-demo-zhrsh.netlify.com/'
            },
            {
                id: 3,
                icon: <FaInstagram className="icon instagram-icon" />,
                path: 'https://gby-demo-zhrsh.netlify.com/'
            }
        ]
    }
    render() {

        return (
            <FooterWrapper>
                <div className="title">damas</div>
                <div className="icons">{
                    this.state.icons.map(icon => (
                        <a href={icon.path} className="icon" key={icon.id} target="_blank" rel="noopener noreferrer">{icon.icon}</a>
                    ))
                }
                </div>
                <p className="copyright"> copyright &copy; 2019 damas</p>
            </FooterWrapper>
        )
    }
}


/**************************************/
//  BannerButton Styled Components
/**************************************/
const FooterWrapper = styled.footer`
padding:2rem 0;
background:${styles.colors.mainBlack};
color:white;
.icons{
    width:10rem;
    display:flex;    
    justify-content:space-between;
    margin:0.6rem auto;
}
.icon{
color:${styles.colors.mainWhite};
font-size:1.6rem;
${styles.transObject({})};
&:hover{
    color:${styles.colors.mainYellow};
}
}
.copyright{
    color:${styles.colors.mainWhite};
    text-transform:capitalize;
    text-align:center;
    font-size:0.9rem;
}
.title{
    /* align the text to center the title dev */
    text-align:center;
    color:${styles.colors.mainYellow};
    /* always when specify width , we need to use margin auto to center the 
    new width div to its parent */
    width:10rem;
    /* the reason of the margin here because we are using specific width which main the
        new width div needs to be centered to the parent div and that is why we use margin 
     top:0rem , auto:left and right, 2rem bottom, again auto left and right */
    margin: 0 auto 2rem auto;

    text-transform:uppercase;
    letter-spacing:0.2rem;
    /* 0.3 top and bottom, 1rem left and right */
    padding:0.3rem 1rem;
    font-size:1.4rem;
    ${styles.border({color:`${styles.colors.mainYellow}`})};
    
}

`;
