import React from "react"
import PropTypes from "prop-types"
import { createGlobalStyle } from 'styled-components';
// import "./layout.css"
import Navbar from '../components/globals/navbar';
import Footer from '../components/globals/Footer';

// import {styles} from '../utils';

const Layout = ({ children }) => {
  return (
    <React.Fragment>
      <GlobalStyle/>
      <Navbar/>
      {children}
      <Footer/>
    </React.Fragment>
  )
}


Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

/*************************/
// Global Styles Component
/*************************/
const tempColor='#f15202';
const GlobalStyle = createGlobalStyle`
*{
  margin: 0%;
  padding: 0%;
  box-sizing: border-box;
  /* border-box; when we add padding to element inside div for example its size remain the same   */
}
:root{
  /* use variable in css start with -- and when u want to use it, 
  to use it write var(--) not supported on all browser*/
  --mainOrange:#f15202;
}

body{
  /* padding: 0rem 2rem; */
  font-family: 'Courier New', Courier, monospace;
  color: grey;
  /* background: ${tempColor}; */
  /* background: var(--mainOrange); */
  background:white;

}
`;

export default Layout
