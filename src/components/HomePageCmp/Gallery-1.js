import React from 'react'
import styled from 'styled-components';
import { styles, Section } from '../../utils';
import { StaticQuery, graphql } from 'gatsby';
import Img from "gatsby-image"

/** Gallery-1: is same like Gallary but with dynamic fetching 
 * of the images using Graphql filter
 * Note: here we need to based our logic on image name or any other 
 * static value that can define a specific image since the images could be fetched randomly
 * */

export default function Gallery() {
    return (<StaticQuery
        query={GET_IMAGES}
        render={data => {
            const images = data.retreiveImgs.edges;
            return (
                <Section>
                    <GallaryWrapper>
                        {images.map(({ node }, index) => {
                            return (
                                <div key={index} className={`item item-${index + 1}`}>
                                    <Img fluid={node.childImageSharp.fluid} />
                                    <p className="info">Awsome Pizza</p>
                                </div>
                            )
                        }
                        )
                        }
                    </GallaryWrapper>
                </Section>
            )
        }
        }
    />
    )
}

/**************************************/
//          Graphql Constants
/**************************************/

const GET_IMAGES = graphql`{
    retreiveImgs: allFile(filter: {relativeDirectory: {eq: "homeGallery"}}) {
     edges{
      node{
        childImageSharp{
          fluid(maxWidth:500){
            src
            aspectRatio

          }
        }
      }
    }
    }
  }
`;


/**************************************/
//  BannerButton Styled Components
/**************************************/

const GallaryWrapper = styled.div`
display:grid;
/* auto here will put all the items in one column */
grid-template-columns:auto;
grid-row-gap:1rem;

.item{
    /* parent div of image and paragraph */
    position:relative;
}
.info{
    /* absolute: take the item from the normal flow so we will be able to place
    whereever we want it on its parent in this cast, top:0 and left:0 */
    position:absolute;
    top:0;
    left:0;
    background:${styles.colors.mainYellow};
    color:${styles.colors.mainBlack};
    padding:0.1rem 0.3rem;
    text-transform:capitalize;
}
/* 576px and up */
@media (min-width:576px){
    /* 2 columns with equal width 1fr: fraction */
    grid-template-columns:1fr 1fr;
    /* 1rem gap from screen width 576 and up we dont need to add it 
    again  */
    grid-column-gap:1rem;

}
@media (min-width:768px){
    /* repeat is shourtcut for grid-template-columns:1fr 1fr 1fr;*/
    grid-template-columns:repeat(3,1fr);
}
@media (min-width:992px){        
    /* one: refres to the first image that cover two rows that is why it will be bigger
    than the two and three, despite they share the same column number but one is spreaded
    on two rows */
    grid-template-areas:
    'one one two two'
    'one one three three';
    .item-1{
    grid-area:one;
    }
    .item-2{
    grid-area:two;
    }
    .item-3{
    grid-area:three;
    }
    /* this below line is to override gatsby default div for this item and we can 
    know the class name by inspecting the page and see the div class name that 
    preventing the image of stretshing to 100% in its container.    
     */
    .gatsby-image-wrapper{
        height:100%;
    }
}



`;
