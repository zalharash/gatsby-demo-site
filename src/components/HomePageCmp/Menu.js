import React from 'react'
import Product from './Product';
import styled from 'styled-components';
import { styles, Section, Title, SectionButton } from '../../utils';
import { StaticQuery, graphql } from 'gatsby';
// import Img from "gatsby-image"
import { Link } from 'gatsby';

export default function Menu() {
    return (
        <Section>
            <Title title="featured item" message="little tast" />
            <ProductList>

                <StaticQuery
                    query={PRODUCTS}
                    render={data => {
                        const prdcts = data.items.edges;
                        return prdcts.map(item => {
                            return <Product key={item.node.id} product={item.node} />
                        })


                    }}
                />

            </ProductList>
        </Section>
    )
}

/**************************************/
//          Graphql Constants
/**************************************/

const PRODUCTS = graphql`
{
    items:allContentfulMenu{
      edges{
        node{
          id
          name
          price
          ingredients
          img{
            fixed(width:150, height:150){
              ...GatsbyContentfulFixed_tracedSVG
            }
          }
        }
      }
    }
  }
`;

/**************************************/
//  BannerButton Styled Components
/**************************************/

const ProductList = styled.div`
margin: 3rem 0;
display:grid;
justify-content:center;
/* Auto or 100% means only one column */
grid-template-columns:100%;
grid-row-gap:3rem;

@media (min-width:576px){
    grid-template-columns:95%;
}
@media (min-width:776){
    grid-template-columns:80%;
    justify-content:center;
}
@media (min-width:992px){
    grid-template-columns:1fr 1fr;
    /* grid-gap for both rows and columns */
    grid-gap:2rem;
}
`;

