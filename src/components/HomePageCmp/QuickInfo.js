import React, { Component } from 'react'
import { Section, Title, SectionButton } from '../../utils'
import styled from 'styled-components';
import { styles } from '../../utils';
import { Link } from 'gatsby';


export default class QuickInfo extends Component {
  render() {
    return (
      <Section>
        <Title message="let us tell you" title="our mission" />
        <QuickInfoWrapper>
          <p className="paragText">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis debitis necessitatibus minus facilis minima iure ex quidem facere quis natus? Debitis velit ad fugit temporibus, nesciunt magnam dolorum quas exercitationem. Ratione quasi doloremque eveniet iusto. Impedit maiores vitae sint aliquam, laborum est nobis reprehenderit aut quam quos cum sequi ab.</p>
          <Link to="/about/" style={{ textDecoration: 'none' }}>
            <SectionButton style={{ margin: '2rem auto' }}> About</SectionButton>
          </Link>
        </QuickInfoWrapper>
      </Section>
    )
  }
}

/**************************************/
//  QuickInfoWrapper Styled Components
/**************************************/
const QuickInfoWrapper = styled.div`
  /* 90% of the 90vw of the section */
  width:90%;
  margin:2rem auto;

  .paragText{
    line-height:1.5rem;
    color:${styles.colors.mainGrey};
    word-spacing:0.2rem;
  }
  @media (min-width:768px) {
    /* to insude that the text stayed centered in the bigger screen 
    we reduced the width*/
    width:80%;
  }
  @media (min-width:992px) {
    width:70%;
  }
`;