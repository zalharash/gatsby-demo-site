import * as styles from './styles';
import {HomeHeader,AboutHeader,MenuHeader,ContactHeader} from './Header';
import Banner from './Banner';
import {BannerButton,SectionButton} from './Button';
import {Section} from './Section';
import {Title} from './Title';


/** instead of including the absolute path to the files inside this folder util
 *  we create this index.js file which will be called automatically in the include
 *  and here we treat it like a placeholder of all the functions and classes inside
 *  util folder (e.g import {HomeHeader} from '../utils/')
 */
export {styles,HomeHeader,AboutHeader,MenuHeader,ContactHeader,Banner,BannerButton,SectionButton,
    Section,Title};