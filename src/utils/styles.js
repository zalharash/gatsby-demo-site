export const colors = {
  mainWhite: `#fff`,
  mainBlack: `#262626`,
  mainYellow: `#d2aa5c`,
  mainYellow2: `#F2AF29`,
  mainGrey: `#474747`,
}
export const textSlanted = `font-family:'Caveat', cursive;`

export const transDefault = 'transition:all 0.5s ease-in-out'
// in case of function if we want to use the default value of the attribute
//we still need to write undefined (transFunction(undefined,'0.4s'))
//or transFunction();
export const transFunction = (
  property = 'all',
  time = '0.5s',
  type = 'linear'
) => {
  return `transition:${property} ${time} ${type}`
}
// in case of object and unlike function if we want to use the default value
// of the attribute we only pass the changed attributes otherwise leave it empty
//(transObject({}) or transObject({time:'1s'}) )
export const transObject = ({
  property = 'all',
  time = '0.5s',
  type = 'ease-in-out',
}) => {
  return `transition: ${property} ${time} ${type}`
}

export const transition = ({
  property = 'all',
  time = '0.5s',
  type = 'ease-in-out',
}) => {
  return `transition: ${property} ${time} ${type}`
}

export const border = ({
  width = '0.15rem',
  type = 'solid',
  color = 'white',
}) => {
  return `border:${width} ${type} ${color}`
}

export const letterSpacing = ({ spacing = '0.1rem' }) => {
  return `letter-spacing:${spacing}`
}
