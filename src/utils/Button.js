import styled from 'styled-components';
import {styles } from '../utils';

//   Here we are not using any state class or function
//   that is why we only exports different buttons here



/**************************************/
//  BannerButton Styled Components
/**************************************/

const BannerButton = styled.button`
/* defined as block to be able to control it   */
/* To horizontally center a block element (like div), use margin: auto */
display: block;
color: ${styles.colors.mainWhite};
background:transparent;
padding:0.5rem 1rem;
text-transform:uppercase;
font-size:1.2rem;
letter-spacing:0.4rem;
font-weight:700;
${styles.border({color:`${styles.colors.mainWhite}`})};
${styles.transObject({})};
&:hover{
    background:${styles.colors.mainWhite};
    color:${styles.colors.mainBlack};
    cursor:pointer;   
}
`;



/**************************************/
//  SectionButton Styled Components
/**************************************/
const SectionButton = styled(BannerButton)`
${styles.border({color:`${styles.colors.mainBlack}`})};
color:${styles.colors.mainBlack};
&:hover{
    background:${styles.colors.mainBlack};
    color:${styles.colors.mainYellow};
    ${styles.transObject({})};
    
}

`;

export {BannerButton,SectionButton};