import React from 'react'
import styled from 'styled-components';
import defaultImg from '../images/bcg/homeBcg.jpeg'


/******************************/
/**    Functions per page     
 *  Hear instead of classes we defined functions in the same Js file
 *  since they have the same CSS and character and then we export them*/
/******************************/
function HomeHeader({ img, children }) {
    return (
        <IndexHeader img={img}>
            {children}
        </IndexHeader>
    )
}
function AboutHeader({ img, children }) {
    return (
        <DefaultHeader img={img}>
            {children}
        </DefaultHeader>
    )
}
function MenuHeader({ img, children }) {
    return (
        <DefaultHeader img={img}>
            {children}
        </DefaultHeader>
    )
}
function ContactHeader({ img, children }) {
    return (
        <DefaultHeader img={img}>
            {children}
        </DefaultHeader>
    )
}

/******************************/
//   CSS Styled Components
/******************************/
const IndexHeader = styled.header`
/* 100% of the screen minus the height of the navbar */
min-height: calc(100vh - 55.78px);
background: linear-gradient(rgba(0,0,0,0.4), rgba(0,0,0,0.4)), 
url(${props => props.img}) center/cover fixed no-repeat;
/* X,Y center  */
display:flex;
justify-content:center;
align-items:center;

color:white;
font-weight:700;
`;

/** all header pages have same attributes like IndexHeader so StyleComp provids
 * such away to reuse the already created component IndexHeader
 */
const DefaultHeader = styled(IndexHeader)`
min-height: 60vh;
`;

/******************************/
//    Default props
/******************************/
HomeHeader.defaultProps ={
    img:defaultImg,
}
AboutHeader.defaultProps ={
    img:defaultImg,
}
MenuHeader.defaultProps ={
    img:defaultImg,
}
ContactHeader.defaultProps ={
    img:defaultImg,
}


export { HomeHeader , AboutHeader,MenuHeader,ContactHeader};