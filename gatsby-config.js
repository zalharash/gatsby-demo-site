/** Enviroment variable 
 * Project environment variables that you defined in the .env.* files will NOT be immediately available in your Node.js scripts. To use those variables, use NPM package dotenv to examine the active .env.* file and attached those values, It’s already a dependency of Gatsby, so you can require it in your gatsby-config.js or gatsby-node.js like this:
 * Now the variables are available on process.env as usual.
 * <img src={`${process.env.GATSBY_API_URL}/logo.png`} alt="Logo" />
 * Netlify will not be able to see the env variables that is why we need
 * to add those there, so it will be able to compile and deploy to Production
 */
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})


/** Gatsby server Restart is a MUST after any change */
module.exports = {
  siteMetadata: {
    title: `XBound`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        // Add any options here
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.CONTENTFUL_SPACE_ID,
        // Learn about environment variables: https://gatsby.app/env-vars
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
